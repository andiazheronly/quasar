import os

AWS_REGION = os.environ.get('AWS_REGION_DEFAULT', 'us-east-1')
DB_TABLE_MESSAGES = os.environ.get("DB_TABLE_PETS", 'messages')
