from pets.exceptions import ApiException
from pets.infrastructure import DBService


class UseCase(object):

    def __init__(self, db_service: DBService):
        self.db = db_service

    def execute(self, **kwargs):
        try:
            return self.process_request(**kwargs)
        except ApiException as ex:
            raise ex
        except BaseException as exc:
            print(
                "{}: {}".format(exc.__class__.__name__, "{}".format(exc)))
            raise exc

    def process_request(self, **kwargs):
        raise NotImplementedError(
            "process_request() not implemented by UseCase class")
