from quasar.exceptions.exceptions import ClientException
from quasar.use_cases import UseCase


class Quasar(UseCase):

    def process_request(self, **kwargs):
        messages = self.db.get_all_messages()
        if not messages:
            raise ClientException('NOT_FOUND', 404)
        return {"messages": messages}
