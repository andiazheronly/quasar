import json
from pets.exceptions import messages


class ApiException(Exception):
    def __init__(self, type, status, value_to_format=None):
        self.type = type
        self.status = status
        self.message = messages.get_message(type, value_to_format)

        self.api_exception = {
            'status': self.status,
            'message': self.message,
            'type': self.type
        }

    def __str__(self):
        return json.dumps(self.api_exception)


# Exception cuando el error es del usuarrio
class ClientException(ApiException):
    pass


# Exception cuando falla el lambda
class LambdaException(ApiException):
    pass


class BadMethodException(ApiException):
    pass


class BadRequestException(ApiException):
    pass


class BadParameterException(ApiException):
    pass
