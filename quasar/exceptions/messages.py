SERVICE_UNAVAILABLE = 'Service is not working'
NOT_FOUND = 'Not found message'
METHOD_NOT_ALLOWED = 'Method not allowed'
BAD_REQUEST = 'Bad request'
NOT_ACCEPTABLE = 'Not Acceptable'

DEFAULT = 'Error message misssing'


def get_message(key, value_to_format=None):
    try:
        message = globals()[key]
        if value_to_format:
            message = message.format(value_to_format)
        return message
    except KeyError:
        return DEFAULT
