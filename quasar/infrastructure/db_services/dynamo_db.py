import boto3 as boto3
from boto3.dynamodb.conditions import Key

from quasar import properties
from quasar.infrastructure.db_services.db_service import DBService

AWS_REGION = properties.AWS_REGION
DB_TABLE_MESSAGES = properties.DB_TABLE_MESSAGES


class DynamoDB(DBService):

    def __init__(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.table_messages = dynamo_db.Table(DB_TABLE_MESSAGES)

    def save_message(self, message):
        response = self.table_messages.put_item(
            Item=message
        )
        return response

    def get_all_messages(self):
        response = self.table_messages.scan()
        return response['Items']

    def get_one_message(self, message_id):
        response = self.table_messages.query(
            KeyConditionExpression=Key('petId').eq(message_id)
        )
        return response['Items']
