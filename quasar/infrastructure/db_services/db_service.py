class DBService:

    def save_message(self, message):
        raise NotImplemented

    def get_all_messages(self):
        raise NotImplemented

    def get_one_message(self):
        raise NotImplemented
